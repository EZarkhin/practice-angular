import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './not-found.component';
import { AuthComponent } from './auth/auth.component';
import { RegisterComponent } from './auth/register.component';
import { PersonalCabComponent } from './personalcab/percab.component';

const appRoutes: Routes = [
    {path: '', redirectTo: '/users', pathMatch: 'full'},
    {path: 'auth', component: AuthComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'personalcabinet', component: PersonalCabComponent},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true} // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
