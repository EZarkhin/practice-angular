import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserListComponent} from './user-list.component';
import {UserDetailComponent} from './user-detail.component';
import {UserService} from './user.service';
import {UserRoutingModule} from './user-routing.module';
import { MapComponent } from '../map/map.component';
import { PersonalCabComponent } from '../personalcab/percab.component';
import { MapService } from '../map/map.service';




@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        UserRoutingModule
    ],
    declarations: [
        UserListComponent,
        UserDetailComponent,
        MapComponent,
        PersonalCabComponent
    ],
    providers: [UserService, MapService]
})
export class UsersModule {
}
