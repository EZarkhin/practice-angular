import 'rxjs/add/operator/switchMap';
import {Component, OnInit, HostBinding} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';

import {User, UserService} from './user.service';
import { locInfo } from '../personalcab/percab.component';
import { MapService } from '../map/map.service';

@Component({
    templateUrl: './user-detail.component.html'
})
export class UserDetailComponent implements OnInit {
    @HostBinding('style.display') display = 'block';
    @HostBinding('style.position') position = 'absolute';
    

    user: User;
    locations: locInfo[];
    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: UserService,
            private MapService: MapService) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => this.loadUser(params));
        this.MapService.getLocationsInfo(+localStorage.getItem('userId')).subscribe(
            location => this.locations = location
        )
    }

    private loadUser(params: Params) {
        this.service.getUser(params['id'])
            .subscribe(
                user => this.user = user,
                err => console.log(err)
            )
    }

    goToUsers(user: User) {
        let id = user ? user.id : null;
        this.router.navigate(['/users', {id}]);
    }

}
