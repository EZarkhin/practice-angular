import { Component } from "@angular/core";
import { User, UserService } from "../users/user.service";
import { AuthService } from "../auth/auth.service";
import { error } from "util";
import { Router, ActivatedRoute, ParamMap, Params } from "@angular/router";
import { MapService, Location } from "../map/map.service";
import { Observable } from "rxjs/Observable";


export type locInfo = {   name: string;  type: string
}

@Component({
    templateUrl: './percab.component.html'
})



export class PersonalCabComponent{
    constructor(
        private UserService: UserService,
        private AuthService: AuthService,
        private router: Router, 
        private MapService: MapService
    ){ }
    isEditState: boolean = false;

    locinfo: locInfo = {
        name: '',
        type: ''
    }
    
    user: User;
    locations: locInfo[];

    ngOnInit() {
        this.UserService.getUser(localStorage.getItem('userId')).subscribe(
           user => this.user = user,
           err => console.log(err)
        )
        this.MapService.getLocationsInfo(+localStorage.getItem('userId')).subscribe(
            location => this.locations = location
        )
        this.MapService.currentLocationName.subscribe(locationName=> this.locinfo.name=locationName);
    }
    
    toggleEditState() {
        this.isEditState = !this.isEditState;
    }

    save(){
        this.UserService.updateUser(this.user).subscribe();
        this.toggleEditState();
        window.location.reload();
    }

    delete(){
        if(confirm('Are you sure?')){
            this.UserService.deleteUser(this.user).subscribe();
            localStorage.clear();
            this.router.navigateByUrl('/auth');
        }
    }
    
    createLocation(){
        
        this.MapService.setInfo(this.locinfo.name, this.locinfo.type);
        this.MapService.createLocation().subscribe();
        this.MapService.canPlaceMarker = true;
        window.location.reload();
        
        
    }

    
    logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        this.router.navigateByUrl('auth');
    }
}