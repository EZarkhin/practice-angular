import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {PageNotFoundComponent} from './not-found.component';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {UsersModule} from './users/users.module';
import {HttpModule} from '@angular/http';
import {HeaderComponent} from './header/header.component'
import { AuthComponent } from './auth/auth.component';
import { FooterComponent } from './footer.component';
import { RegisterComponent } from './auth/register.component';
import { AuthService } from './auth/auth.service';
import { MapService } from './map/map.service';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        UsersModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        HeaderComponent,
        AuthComponent,
        FooterComponent,
        RegisterComponent   
    ],
    providers: [AuthService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
