import { Component } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})

export class HeaderComponent{
    constructor(private service: AuthService,
                private router: Router){
        
    }

    isLoggedIn(){
        return this.service.isLoggedIn();
    }

    logout(){
        this.service.logout();
        this.router.navigateByUrl('/auth')
    }
}