import { Component } from "@angular/core";
import { AuthService } from "./auth.service";
import { error } from "selenium-webdriver";
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';
@Component({
    templateUrl: './auth.component.html'
})

export class AuthComponent{
    constructor(
        private service: AuthService,
        private route: ActivatedRoute,
        private router: Router
       ) {}
       name='';
       password='';
    auth()
       {
          this.service.authUser(this.name, this.password).subscribe(
              data =>{
                    localStorage.setItem('token', data.token)
                    localStorage.setItem('userId', data.userId)
                    this.router.navigateByUrl('/');
              },
              error => console.error(error)
          ) 
       }
}