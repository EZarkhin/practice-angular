import { Component } from "@angular/core";
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';
import { AuthService} from "./auth.service";
import { User } from "../users/user.service";


@Component({
    templateUrl: "./register.component.html"
})

export class RegisterComponent{
    constructor(private route: ActivatedRoute,
        private router: Router,
        private service: AuthService
       ) {}
       user: User = {id: 0, name: '', password: '', age: 0, occupation: '' }
    
    register(){
        this.service.registerUser(this.user).subscribe();
        this.router.navigate(['/users']);
    }
}