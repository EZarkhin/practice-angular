import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {ResponseContentType} from '@angular/http/src/enums';
import { User } from '../users/user.service';




@Injectable()
export class AuthService {
    private userUrl = 'http://localhost:8000/api/v1/auth/';
    constructor(private http: Http) {
    }
    

    registerUser(user: User){
        return this.http.post(`${this.userUrl}register`, user)

        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    authUser(name: string, password: string){
        return this.http.post(`${this.userUrl}`, {name: name, password: password})
        .map((responce: Response) => responce.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    
    }

    logout(){
        localStorage.clear();
    }

    isLoggedIn(){
        return localStorage.getItem('token') !== null
    }

}
