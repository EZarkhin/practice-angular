import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ElementRef} from '@angular/core';
import {} from '@types/googlemaps';
import { MapService } from './map.service';
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';

type Pos = { lat: number; lng: number };

@Component({
    selector: 'g-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
    @ViewChild('mapCanvas') mapCanvas: ElementRef;

    private pos: Pos = {
        lat: 46.4598865,
        lng: 30.5717048
    };

    
    
    private map: any;
    private geocoder: any;
    constructor(
        private service: MapService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }
    
    ngOnInit() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((pos) => {
                this.pos.lat = pos.coords.latitude;
                this.pos.lng = pos.coords.longitude;
                if (this.router.url==='/personalcabinet')
                this.paintMarkers(+localStorage.getItem('userId'))
                else {
                    this.paintMarkers(+this.route.snapshot.params['id'])
                    this.service.canPlaceMarker=false}
            });
        }
    }

    static _mapCenter(lat:number, lng:number) {
        return new google.maps.LatLng(lat, lng)
    }

    _placeMarker(location:Pos) {
        return new google.maps.Marker({
            position: location,
            map: this.map,
           
        });
    }

    paintMarkers(id: number){
        this.service.getLocationsPos(id)
            .subscribe(
                positions => positions.forEach( item =>{
                    this._placeMarker(item);})
            )
    }

    ngAfterViewInit() {
        this.map = new google.maps.Map(this.mapCanvas.nativeElement, {
            zoom: 11,
            center: MapComponent._mapCenter(this.pos.lat, this.pos.lng),
            disableDefaultUI: true
        });
        
        this.map.addListener('click', (event: google.maps.MouseEvent) => {
            if(this.service.canPlaceMarker){
                this.service.canPlaceMarker=false;
            this._placeMarker({lat: event.latLng.lat(), lng: event.latLng.lng()})
            this.service.setPos({lat: event.latLng.lat(), lng: event.latLng.lng()})
            this.geocoder = new google.maps.Geocoder().geocode({location: {lat: event.latLng.lat(), lng: event.latLng.lng()}}, (results, status) =>{
                if (status === google.maps.GeocoderStatus.OK) {
            
                        new google.maps.places.PlacesService(this.map).getDetails({placeId: results[0].place_id}, (place, placeStatus) =>{
                            if(placeStatus === google.maps.places.PlacesServiceStatus.OK){
                                this.service.changeLocationName(place.name);
                            }
                        })
                    
            }})}
        });
    }
}
