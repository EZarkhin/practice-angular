import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {ResponseContentType} from '@angular/http/src/enums';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class Location {
    constructor(public id: number,
                public name: string,
                public type: string,
                public lat: number,
                public lng: number,
                public userId: number) {
    }
}

type Pos = { lat: number; lng: number };

@Injectable()
export class MapService {
    private locationUrl = 'http://localhost:8000/api/v1/locations/';

    constructor(private http: Http) {
    }
    
    canPlaceMarker = true;

    location: Location ={
        id: 0,
        name: '',
        type: '',
        lat: 0,
        lng: 0,
        userId: +localStorage.getItem('userId')
    };

    private locationNameSource = new BehaviorSubject<string>('Default name');
    currentLocationName = this.locationNameSource.asObservable();

    changeLocationName(name: string){
        this.locationNameSource.next(name);
    }

    setPos(pos: Pos){
        this.location.lat = pos.lat;
        this.location.lng = pos.lng; 
    }

    setInfo(name: string, type: string){
        this.location.name=name;
        this.location.type = type;
    }

    getLocationsPos(userId: number | string): Observable<Pos[]> {
        return this.http.get(`${this.locationUrl}pos/${+userId}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getLocationsInfo(userId: number | string): Observable<Location[]> {
        return this.http.get(`${this.locationUrl}info/${+userId}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getLocation(id: number | string) {
        return this.http.get(`${this.locationUrl}${+id}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    createLocation(){
        const token = localStorage.getItem('token')
        ? '?token=' + localStorage.getItem('token')
        : '';
        return this.http.post(`${this.locationUrl}create${token}`, this.location)
        
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    updateLocation(location: Location) {
        return this.http.put(`${this.locationUrl}${+location['id']}`, location)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
}